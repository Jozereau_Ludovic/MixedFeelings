<h1>Projet de cours UQAC - Mixed Feelings</h1>

<p><h2>Description du travail</h2>
Mixed Feeling est un Puzzle Game à la 1ère personne dans lequel on incarne un
robot devant résoudre des salles de test. 
</p>

<p><h2>Technologies et langages utilisés</h2>
<ul><li>Visual Studio
    <li>Unreal Engine 4
    <li>Blueprint
    <li>C++
    <li>Git
</ul></p>

<p><h2>Méthode de travail</h2>
<ul><li>Méthodologie Agile : SCRUM
</ul></p>

<p><h2>Rôles dans le projet</h2>
<ul><li>Game designer
    <li>Level designer
    <li>Programmeur
</ul></p>

<p><h2>Participants</h2>
<ul><li>Stiven Aigle
    <li>Ludovic Jozereau
    <li>Benjamin Laschkar
    <li>Victor Mottet
    <li>Yann Klemens Ragueneau
</ul></p>

[Accès vers l'exécutable](https://drive.google.com/drive/folders/1XHtTR5jC654m7lwEamsyX4VTd4qZuNK-?usp=sharing)