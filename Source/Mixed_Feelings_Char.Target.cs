// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class Mixed_Feelings_CharTarget : TargetRules
{
	public Mixed_Feelings_CharTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;

		ExtraModuleNames.AddRange( new string[] { "Mixed_Feelings_Char" } );
	}
}
