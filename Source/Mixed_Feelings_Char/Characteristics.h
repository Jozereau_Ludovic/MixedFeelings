 // Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <vector>
#include "Feeling.h"
#include "CoreMinimal.h"
#include "Engine.h"
#include "RobotCharacter.h"
#include <string>
#include "Characteristics.generated.h"

UENUM(BlueprintType)
enum class Feelings: uint8{
	JOY, ANGER, SADNESS, FEAR, DISGUST, NONE
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MIXED_FEELINGS_CHAR_API UCharacteristics : public UActorComponent
{
	GENERATED_BODY()

private:
	std::vector<UFeeling*> feelings;
	UFeeling * currentFeeling;
	ARobotCharacter * player;

	bool sad;
	bool angry;

	bool canAct = true;

	
public:	
	void emotionStillAvaiblable();

	// Sets default values for this component's properties
	UCharacteristics();
	~UCharacteristics();
	
	UFUNCTION(BlueprintCallable, Category = "feel")
		void ActiveEffect1();
	UFUNCTION(BlueprintCallable, Category = "feel")
		void ActiveEffect2();
	UFUNCTION(BlueprintCallable, Category = "feel")
		bool ChangeFeeling(int emotionNumber);
	UFUNCTION(BlueprintCallable, Category = "feel")
		void UpdateFeelingsLevel(bool joy, bool ang, bool sadness, bool fear, bool disgust);

	UFUNCTION(BlueprintCallable, Category = "feel")
		void InitFeelingsLevel(int joy, int ang, int sadness, int fear, int disgust);

	UFUNCTION(BlueprintCallable, Category = "feel")
	int const & GetJoyLevel() const;
	UFUNCTION(BlueprintCallable, Category = "feel")
	int const & GetAngerLevel() const;
	UFUNCTION(BlueprintCallable, Category = "feel")
	int const & GetSadnessLevel() const;
	UFUNCTION(BlueprintCallable, Category = "feel")
	int const & GetFearLevel() const;
	UFUNCTION(BlueprintCallable, Category = "feel")
	int const & GetDisgustLevel() const;
	UFUNCTION(BlueprintCallable, Category = "feel")
	bool const IsAngry() const;
	UFUNCTION(BlueprintCallable, Category = "feel")
		void AddFeeling(Feelings feeling);
	UFUNCTION(BlueprintCallable, Category = "feel")
		void StopPassive();

	bool IsSad() const;

	void setCanAct(bool can);

	bool hasFeeling() const;

	FString GetEmotionsNames(std::vector<bool> emotions) const;
};
