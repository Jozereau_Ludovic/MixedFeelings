// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Activable.h"
#include "AbstractButton.h"
#include "Components/BoxComponent.h"
#include <set>
#include "PressurePlate.generated.h"

/**
 * 
 */
UCLASS()
class MIXED_FEELINGS_CHAR_API APressurePlate : public AAbstractButton
{
	GENERATED_BODY()


		bool once = true;
	public: 
	APressurePlate();

	std::set<AActor *> actorOnPlate;
	UPROPERTY(EditAnywhere, Category = "MassNeeded")
	float TotalMassNeededToActivate = 30;

	float ActualMass = 0;
	
	UPROPERTY(EditInstanceOnly, Category = "timeActivation")
	float timeActive = 0;

	float count = 0;

	void BeginPlay() override;
	bool active = false;
	UFUNCTION(BlueprintCallable)
	float getActualMasse();

	UFUNCTION(BlueprintCallable)
	void setActualMasse(float mass);
	
	UFUNCTION()
	void Tick(float delta) override;

	UFUNCTION(BlueprintCallable, Category = "Buttons")
	void PressButton() override;
	UFUNCTION(BlueprintCallable, Category = "Buttons")
	void PressParentButton();
	
	UFUNCTION(BlueprintCallable)
		float getTotalMassNeededToActivate();
	
	UFUNCTION(BlueprintCallable)
		void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, 
		AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION(BlueprintCallable)
		void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) ;



	UFUNCTION(BlueprintNativeEvent)
	void colorOn();

	UFUNCTION(BlueprintNativeEvent)
	void colorOff();		
}; 
