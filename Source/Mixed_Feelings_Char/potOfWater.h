// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbstractButton.h"
#include "Components/SphereComponent.h"
#include "GameFramework/Actor.h"
#include "potOfWater.generated.h"

UCLASS()
class MIXED_FEELINGS_CHAR_API ApotOfWater : public AAbstractButton
{
	GENERATED_BODY()
	
public :
	ApotOfWater();
	void PressButton() override;
	bool onlyOnce = false;
	AActor *a;

	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent *OverlappedComp, AActor *OtherActor,
		UPrimitiveComponent *OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult);

	UFUNCTION(BlueprintCallable)
		void setOnlyOnce(bool onlyOnce);

	UFUNCTION(BlueprintCallable)
		bool getOnlyOnce();
	
	UPROPERTY(VisibleAnywhere, Category = "Sphere collision")
		class USphereComponent *SphereComp;
	UPROPERTY(EditAnywhere, Category = "SizeForSphere")
		float sizeForSphere = 100.0f;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

};
