// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/EngineTypes.h"
#include "CoreMinimal.h"
#include "../RobotCharacter.h"
/**
 * 
 */
class MIXED_FEELINGS_CHAR_API RayCast
{
private:
	RayCast();
public:
	static bool RayCastFromPlayerPointOfView(ARobotCharacter * robot, FHitResult &Hit, float sizeRay = 300 );
	~RayCast();
};
