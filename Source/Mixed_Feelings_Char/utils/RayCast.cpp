// Fill out your copyright notice in the Description page of Project Settings.

#include "RayCast.h"
#include "DrawDebugHelpers.h"
RayCast::RayCast()
{
}

RayCast::~RayCast()
{
}

bool RayCast::RayCastFromPlayerPointOfView(ARobotCharacter *robot, FHitResult &Hit,  float sizeRay)
{
    FVector CamLoc;
    FRotator CamRot;

    robot->GetController()->GetPlayerViewPoint(CamLoc, CamRot); // Get the camera position and rotation
    FVector StartTrace = CamLoc + 50*CamRot.Vector();                          // trace start is the camera location
    const FVector Direction = CamRot.Vector();
    //StartTrace -= Direction*100;
    const FVector EndTrace = StartTrace + Direction * sizeRay; // and trace end is the camera location + an offset in the direction you are looking, the 200 is the distance at wich it checks
   // DrawDebugLine(robot->GetWorld(), StartTrace, EndTrace, FColor(255, 0, 0), false, .1f, 0, 12.3f);
    FCollisionObjectQueryParams params;
    return robot->GetWorld()->LineTraceSingleByObjectType(Hit, StartTrace, EndTrace, params);
}