// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SphereComponent.h"
#include "Activable.h"
#include <vector>
#include "AbstractButton.generated.h"

UCLASS()
class MIXED_FEELINGS_CHAR_API AAbstractButton : public AActor
{
	GENERATED_BODY()
protected:
	static int constexpr maxButtons = 10 ;
public:	
	// Sets default values for this actor's properties
	UPROPERTY(EditAnywhere, Category = "ActivatableObject")
	AActivable * activatableObject[maxButtons];
	AAbstractButton();
	UPROPERTY(EditAnywhere, Category = "Size")
		float size = 100.0f;
	UPROPERTY(EditAnywhere, Category = "Inverter")
		bool inverter = false;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, Category = "Buttons")
	virtual void PressButton();

	UFUNCTION(BlueprintNativeEvent, Category = "Buttons")
	void doSomethingAfterPress();

	UPROPERTY(VisibleAnywhere, Category = "Buttons")
	class USphereComponent* ButtonHitbox;

	
};
