// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Feeling.h"

/**
 * 
 */
class MIXED_FEELINGS_CHAR_API UDisgustFeeling : public UFeeling
{

public:

	UDisgustFeeling(ACharacter * player):UFeeling(player){}
	virtual void PassiveEffect() override;
	virtual void ActiveEffect1() override;
	virtual void ActiveEffect2() override;
	
		
	
	
	
};
