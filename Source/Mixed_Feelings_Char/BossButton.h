// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TimerButton.h"
#include "BossButton.generated.h"

/**
 * 
 */
UCLASS()
class MIXED_FEELINGS_CHAR_API ABossButton : public ATimerButton
{
	GENERATED_BODY()
	public:
	UPROPERTY(EditAnywhere, Category = "ActivatableObject")
	AActivable * activatableObject1[maxButtons];
	UPROPERTY(EditAnywhere, Category = "ActivatableObject")
	AActivable * activatableObject2[maxButtons];
	UPROPERTY(EditAnywhere, Category = "ActivatableObject")
	AActivable * activatableObject3[maxButtons];
	
	int state = 0;
	void Tick(float DeltaTime) override;
	
	
};
