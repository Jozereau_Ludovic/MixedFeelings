// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Mixed_Feelings_CharGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class MIXED_FEELINGS_CHAR_API AMixed_Feelings_CharGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
		virtual void StartPlay() override;
	
	
};

