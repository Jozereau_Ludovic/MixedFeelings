// Fill out your copyright notice in the Description page of Project Settings.

#include "PressurePlate.h"
#include "RobotCharacter.h"

APressurePlate::APressurePlate() {
	PrimaryActorTick.bCanEverTick = true;
     PrimaryActorTick.bStartWithTickEnabled = true;

	
}
float APressurePlate::getActualMasse() {
	return ActualMass;
}
void APressurePlate::setActualMasse(float mass) {
	ActualMass = mass;
}
float APressurePlate::getTotalMassNeededToActivate() {
	return TotalMassNeededToActivate;
}
void APressurePlate::BeginPlay() {
	Super::BeginPlay();
	if (active){
		colorOn();
	}else {
		colorOff();		
	}
}
void APressurePlate::PressButton() {

}
void APressurePlate::PressParentButton() {
	if (once){
		once = false;
		AAbstractButton::PressButton();
	}
}

void APressurePlate::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, 
		AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult){
		actorOnPlate.insert(OtherActor);
		
}

void APressurePlate::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) {
	auto it = actorOnPlate.find(OtherActor);
	
	if(it != actorOnPlate.end()){
		actorOnPlate.erase(it);
	}
}
void APressurePlate::colorOn_Implementation(){

}

void APressurePlate::colorOff_Implementation(){

}

void APressurePlate::Tick(float delta) {
	Super::Tick(delta);
	float mass = 0;
	for(auto actor : actorOnPlate){
		
		if(Cast<ARobotCharacter>(actor)){
			mass += Cast<ARobotCharacter>(actor)->getPlayerWeight();
		}else{
			mass += Cast<UPrimitiveComponent>(actor->GetRootComponent())->GetMass();
		}
	}
	if (!active && mass >= TotalMassNeededToActivate){
		active = true;
		AAbstractButton::PressButton();
		colorOn();
	}else if(mass <= TotalMassNeededToActivate && active){
		if(count >= timeActive){
			active = false;
			AAbstractButton::PressButton();
			colorOff();		
			count = 0;
		}else{
			count += delta;
		}
	}
}
