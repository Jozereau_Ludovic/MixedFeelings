// Fill out your copyright notice in the Description page of Project Settings.

#include "FearFeeling.h"
#include "Engine.h"
#include "RobotCharacter.h"
void UFearFeeling::PassiveEffect(){
	ARobotCharacter * robot = Cast<ARobotCharacter>(player);
	// player->GetCharacterMovement()->MaxWalkSpeed += 1000;
	robot->ChangeMaxSpeed(1000);
	Cast<ARobotCharacter>(player)->setWeight(300);
	
}
void UFearFeeling::StopPassiveEffect(){
	// player->GetCharacterMovement()->MaxWalkSpeed -= 1000;
	ARobotCharacter * robot = Cast<ARobotCharacter>(player);
	robot->BeVisible();
	robot->ChangeMaxSpeed(-1000);
}
void UFearFeeling::ActiveEffect1(){

	ARobotCharacter * robot = Cast<ARobotCharacter>(player);
	robot->ChangeVisibility();

	
}
void UFearFeeling::ActiveEffect2(){

	ARobotCharacter * robot = Cast<ARobotCharacter>(player);
	robot->setCanWalkOnWater(true);
	robot->setImpulseValue(5000000);
	robot->BeginDash();
}

