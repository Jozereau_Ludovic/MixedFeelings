// Fill out your copyright notice in the Description page of Project Settings.

#include "RobotCharacter.h"
#include "Engine.h"
#include "TransformNonVectorized.h"
#include "utils/RayCast.h"
#include <iostream>
#include "Characteristics.h"
#include "potOfWater.h"
std::vector<int> ARobotCharacter::characInit{0, 0, 0, 0, 0};
// Sets default values
ARobotCharacter::ARobotCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create a first person camera component.
	FPSCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	// Attach the camera component to our capsule component.
	FPSCameraComponent->SetupAttachment(GetCapsuleComponent());
	// Position the camera slightly above the eyes.
	FPSCameraComponent->SetRelativeLocation(FVector(0.0f, 0.0f, 50.0f + BaseEyeHeight));
	// Allow the pawn to control camera rotation.
	FPSCameraComponent->bUsePawnControlRotation = true;

	// Create a first person mesh component for the owning player.
	FPSMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FirstPersonMesh"));
	// Only the owning player sees this mesh.
	FPSMesh->SetOnlyOwnerSee(true);
	// Attach the FPS mesh to the FPS camera.
	FPSMesh->SetupAttachment(FPSCameraComponent);
	// Disable some environmental shadowing to preserve the illusion of having a single mesh.
	FPSMesh->bCastDynamicShadow = false;
	FPSMesh->CastShadow = false;
	// The owning player doesn't see the regular (third-person) body mesh.
	GetMesh()->SetOwnerNoSee(true);

	// BUTTONS
	// Add Buttons detection hitbox
	TriggerCapsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Trigger Capsule"));
	// Set capsule size. Should match the player's range
	TriggerCapsule->InitCapsuleSize(55.f, 96.f);
	TriggerCapsule->SetCollisionProfileName(TEXT("Trigger"));
	TriggerCapsule->SetupAttachment(RootComponent);
	// Automatically call the corresponding functions when other hitboxes overlap with the trigger capsule.
	TriggerCapsule->OnComponentBeginOverlap.AddDynamic(this, &ARobotCharacter::OnOverlapBegin);
	TriggerCapsule->OnComponentEndOverlap.AddDynamic(this, &ARobotCharacter::OnOverlapEnd);
	// When starting a level, there are no buttons to activate
	CurrentButton = nullptr;
	characteristics = CreateDefaultSubobject<UCharacteristics>(TEXT("J'en ai marre d'unreal..."));
	characteristics->InitFeelingsLevel(characInit[0], characInit[1], characInit[2], characInit[3], characInit[4]);
	mustChange = true;
}
ARobotCharacter::~ARobotCharacter()
{
}
bool ARobotCharacter::hasFeeling() const
{

	return characteristics->hasFeeling();
}

// Called when the game starts or when spawned
void ARobotCharacter::BeginPlay()
{
	Super::BeginPlay();

	maxSpeed = GetCharacterMovement()->MaxWalkSpeed;
}

// Called every frame
void ARobotCharacter::Tick(float DeltaTime)
{
	if (mustChange)
	{
		for (int j = 0; j < 4; j++)
		{
			if (characInit[j])
			{
				ChangeFeelingsUI(j);
				mustChange = false;
				break;
			}
		}
	}

	Super::Tick(DeltaTime);
	if (ObjectGrab)
	{
		FVector Direction = Controller->GetControlRotation().Vector();
		Direction.Normalize();
		ObjectGrab->K2_TeleportTo(GetTransform().GetTranslation() + 100 * GetActorForwardVector() + 30 * GetActorUpVector(), GetActorRotation());
	}
	if (maxSpeed < GetCharacterMovement()->MaxWalkSpeed)
	{
		GetCharacterMovement()->MaxWalkSpeed -= 9;
	}
	if (!characteristics->hasFeeling())
	{
		weight = 300;
	}
	Dash(DeltaTime);
}

void ARobotCharacter::ChangeMaxSpeed(float value)
{
	maxSpeed += value;
	if (value > 0)
	{
		GetCharacterMovement()->MaxWalkSpeed += value;
	}
}
void ARobotCharacter::BeInvisible_Implementation()
{
	invisible = true;
}

void ARobotCharacter::BeVisible_Implementation()
{
	invisible = false;
}

void ARobotCharacter::ChangeVisibility()
{
	if (!IsInvisible())
	{
		BeInvisible();
	}
	else
	{
		BeVisible();
	}
}
bool ARobotCharacter::IsInvisible() const
{
	return invisible;
}

void ARobotCharacter::Grab(AGrabableObject *grabable)
{
	if (grabAllowed)
	{
		ObjectGrab = grabable;
		if (ObjectGrab)
		{
			ObjectGrab->ChangeGravity();
		}
	}
}
void ARobotCharacter::canGrab(bool can)
{
	grabAllowed = can;
}

void ARobotCharacter::UnGrab()
{
	ObjectGrab = nullptr;
}

void ARobotCharacter::GrabOrUnGrab()
{
	if (HasObject())
	{
		ObjectGrab->ChangeGravity();
		UnGrab();
	}
	else
	{
		FHitResult Hit;
		if (RayCast::RayCastFromPlayerPointOfView(this, Hit))
		{
			if (Hit.GetActor())
			{
				if (Hit.GetActor()->GetClass()->IsChildOf(AGrabableObject::StaticClass()))
				{
					if (Cast<UPrimitiveComponent>(Hit.GetActor()->GetRootComponent())->GetMass() <= 300 || characteristics->IsAngry())
					{
						Grab(Cast<AGrabableObject>(Hit.GetActor()));
					}
				}
			}
		}
	}
}
void ARobotCharacter::Launch()
{
	ObjectGrab->ChangeGravity();
	ObjectGrab->launch(60000000 * GetActorForwardVector());
	UnGrab();
}
bool ARobotCharacter::HasObject()
{
	return (bool)ObjectGrab;
}

// Called to bind functionality to input
void ARobotCharacter::SetupPlayerInputComponent(UInputComponent *PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	// Set up "movement" bindings.
	PlayerInputComponent->BindAxis("MoveForward", this, &ARobotCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ARobotCharacter::MoveRight);

	// Set up "look" bindings.
	PlayerInputComponent->BindAxis("Turn", this, &ARobotCharacter::AddYaw);
	PlayerInputComponent->BindAxis("LookUp", this, &ARobotCharacter::AddPitch);

	// Set up "action" bindings.
	//PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ARobotCharacter::StartJump);
	//PlayerInputComponent->BindAction("Jump", IE_Released, this, &ARobotCharacter::StopJump);

	// ACTION
	PlayerInputComponent->BindAction("Action", IE_Pressed, this, &ARobotCharacter::Action);
	// PlayerInputComponent->BindAction("Grab", IE_Pressed, this, &ARobotCharacter::GrabOrUnGrab);

	PlayerInputComponent->BindAction("emotion2", IE_Pressed, this, &ARobotCharacter::emotion2);
	PlayerInputComponent->BindAction("emotion1", IE_Pressed, this, &ARobotCharacter::emotion1);
	PlayerInputComponent->BindAction("emotion3", IE_Pressed, this, &ARobotCharacter::emotion3);
	PlayerInputComponent->BindAction("emotion4", IE_Pressed, this, &ARobotCharacter::emotion4);
	PlayerInputComponent->BindAction("emotion5", IE_Pressed, this, &ARobotCharacter::emotion5);
}
void ARobotCharacter::AddYaw(float rate)
{
	if (canWalk)
	{
		AddControllerYawInput(rate);
	}
}
void ARobotCharacter::AddPitch(float rate)
{
	if (canWalk)
	{
		AddControllerPitchInput(rate);
	}
}
void ARobotCharacter::MoveForward(float Value)
{
	// Find out which way is "forward" and record that the player wants to move that way.
	if (canWalk)
	{
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void ARobotCharacter::MoveRight(float Value)
{
	// Find out which way is "right" and record that the player wants to move that way.
	if (canWalk)
	{
		FVector Direction = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::Y);
		AddMovementInput(Direction, Value);
	}
}

void ARobotCharacter::StartJump()
{
	// Robot can not always jump
	bPressedJump = true;
}

void ARobotCharacter::StopJump()
{
	bPressedJump = false;
}

// Activates the nearest button if it's in range
void ARobotCharacter::Action()
{
	// Buttons must be in range
	if (CurrentButton)
	{
		CurrentButton->PressButton();
	}
	GrabOrUnGrab();
}

// Set the current button to activate when player gets close
void ARobotCharacter::OnOverlapBegin(UPrimitiveComponent *OverlappedComp, AActor *OtherActor, UPrimitiveComponent *OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult)
{
	if (OtherActor && (OtherActor != this) && OtherComp && OtherActor->GetClass()->IsChildOf(AAbstractButton::StaticClass()))
	{
		if(!Cast<ApotOfWater>(OtherActor)){
			CurrentButton = Cast<AAbstractButton>(OtherActor);
		}
	}
}
void ARobotCharacter::setCanWalk(bool walk)
{
	canWalk = walk;
	characteristics->setCanAct(walk);
}
// Unset the current button to activate when player gets close
void ARobotCharacter::OnOverlapEnd(UPrimitiveComponent *OverlappedComp, AActor *OtherActor, UPrimitiveComponent *OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor && (OtherActor != this) && OtherComp)
	{
		CurrentButton = nullptr;
	}
}

void ARobotCharacter::emotion1()
{
}
void ARobotCharacter::emotion2()
{
}
void ARobotCharacter::emotion3()
{
}
void ARobotCharacter::emotion4()
{
}
void ARobotCharacter::emotion5()
{
}
void ARobotCharacter::Dash(float delta)
{
	static float timeDashing = 0;

	if (dash)
	{
		GetCharacterMovement()->AddImpulse(GetActorForwardVector() * impulse_value);
		timeDashing += delta;
		AddMovementInput(GetActorForwardVector(), 1);
		if (timeDashing > maxTimeDashing)
		{
			dash = false;
			timeDashing = 0;
		}
		//Get the static mesh of the chosen Actor
	}
}
void ARobotCharacter::BeginDash()
{
	dash = true;
}
void ARobotCharacter::SpawnPlant(FTransform const &trans)
{
	if (PlantBP)
	{
		//Spawn parameters for the current spawn.
		//We can use this for a number of options like disable collision or adjust the spawn position
		//if a collision is happening in the spawn point etc..
		FActorSpawnParameters SpawnParams;

		//Actual Spawn. The following function returns a reference to the spawned actor
		GetWorld()->SpawnActor<APlant>(PlantBP, trans, SpawnParams);

		GLog->Log("Spawned the UsefulActor.");
	}
}

void ARobotCharacter::SpawnPlantToGrow(FTransform const &trans)
{
	if (PlantToGrowBP)
	{
		//Spawn parameters for the current spawn.
		//We can use this for a number of options like disable collision or adjust the spawn position
		//if a collision is happening in the spawn point etc..
		FActorSpawnParameters SpawnParams;

		//Actual Spawn. The following function returns a reference to the spawned actor
		APlant *plant = GetWorld()->SpawnActor<APlant>(PlantToGrowBP, trans, SpawnParams);
		plant->Grow();
		GLog->Log("Spawned the UsefulActor.");
	}
}

int ARobotCharacter::getPlayerWeight()
{
	float mass = weight;
	if (ObjectGrab)
	{
		mass += Cast<UPrimitiveComponent>(ObjectGrab->GetRootComponent())->GetMass();
	}
	return mass;
}

// Displays the game over screen for overdose. Message to display should be given as parameter.
void ARobotCharacter::OverdoseGameOver_Implementation(const FString &gameOverMessage)
{
	// Nothing to do, all blueprint UI
}
void ARobotCharacter::CanPassThroughWall_Implementation()
{
}

void ARobotCharacter::CannotPassThroughWall_Implementation()
{
}
void ARobotCharacter::ChangeFeelingsUI_Implementation(int emotion)
{
}

void ARobotCharacter::setCanWalkOnWater(bool new_value)
{
	canWalkOnWater = new_value;
}
bool ARobotCharacter::getCanWalkOnWater()
{
	return canWalkOnWater;
}

void ARobotCharacter::setImpulseValue(int impulse_value_set)
{
	impulse_value = impulse_value_set;
}

void ARobotCharacter::InitCharacsForLevel(int joy, int anger, int sadness, int fear, int disgust)
{
	characteristics->InitFeelingsLevel(joy, anger, sadness, fear, disgust);
	characInit[0] = joy;
	characInit[1] = anger;
	characInit[2] = sadness;
	characInit[3] = fear;
	characInit[4] = disgust;
}

bool ARobotCharacter::IsDashing() const
{
	return dash;
}

void ARobotCharacter::setCanInteractWithPotOfWater(bool value_canInteractWithPotOfWater)
{
	canInteractWithPotOfWater = value_canInteractWithPotOfWater;
}

bool ARobotCharacter::getCanInteractWithPotOfWater()
{
	return canInteractWithPotOfWater;
}