// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Components/ActorComponent.h"

class ACharacter;

class MIXED_FEELINGS_CHAR_API UFeeling
{
protected:
	ACharacter * player;
	int feelingLevel;
	// 0 : Not affectable 
	// 1 : Can affect player, passive will be used
	// 2+: First active is usable 
	// 4+: Second active is usable
	// 6+: Overdose
	bool overdose;

public:
	UFeeling(ACharacter * player);

	virtual void PassiveEffect(){}
	virtual void StopPassiveEffect(){}	
	virtual void ActiveEffect1() = 0;
	virtual void ActiveEffect2() = 0;
		
	void SetFeelingLevel(int level);
	UFUNCTION(BlueprintCallable, Category = "LevelFeelings")
	int const & GetFeelingLevel() const;

	UFUNCTION(BlueprintCallable, Category = "LevelFeelings")
	bool const & GetOverdose() const;
	
};
