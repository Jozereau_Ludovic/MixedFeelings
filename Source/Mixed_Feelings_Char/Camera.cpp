// Fill out your copyright notice in the Description page of Project Settings.

#include "Camera.h"
#include "RobotCharacter.h"
#include "Engine.h"

// Sets default values
ACamera::ACamera()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Add detection hitbox
	CapsuleTrigger = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Capsule Trigger"));
	
	// Set capsule size. Should match the player's range
	CapsuleTrigger->InitCapsuleSize(55.f, 96.f);
	CapsuleTrigger->SetCollisionProfileName(TEXT("Trigger"));
	CapsuleTrigger->SetupAttachment(RootComponent);

	// Automatically call the corresponding functions when other hitboxes overlap with the trigger capsule.
	CapsuleTrigger->OnComponentBeginOverlap.AddDynamic(this, &ACamera::OnOverlapBegin);
	CapsuleTrigger->OnComponentEndOverlap.AddDynamic(this, &ACamera::OnOverlapEnd);
}

// Called when the game starts or when spawned
void ACamera::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACamera::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ACamera::toggleAction_Implementation() {
	active = !active;
}

void ACamera::On_Implementation() {
	active = true;
}
void ACamera::Off_Implementation() {
	active = false;
}

// Set the current button to activate when player gets close
void ACamera::OnOverlapBegin(UPrimitiveComponent *OverlappedComp, AActor *OtherActor, UPrimitiveComponent *OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult)
{
	if (active && !triggered) {
		auto a = Cast<ARobotCharacter>(OtherActor);

		if (a && !a->IsInvisible()) {
			triggered = true;

			if (Button) {
				Button->PressButton();
			}
		}
	}
}

// Unset the current button to activate when player gets close
void ACamera::OnOverlapEnd(UPrimitiveComponent *OverlappedComp, AActor *OtherActor, UPrimitiveComponent *OtherComp, int32 OtherBodyIndex)
{
	if (active && triggered) {
		auto a = Cast<ARobotCharacter>(OtherActor);

		if (a) {
			triggered = false;
			if (Button) {
				Button->PressButton();
			}
		}
	}
}

bool ACamera::IsActive() const {
	return active;
}

bool ACamera::IsTriggered() const {
	return triggered;
}