// Fill out your copyright notice in the Description page of Project Settings.

#include "potOfWater.h"
#include "RobotCharacter.h"
#include "Engine.h"

// Sets default values
ApotOfWater::ApotOfWater()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Add detection hitbox
	SphereComp = CreateDefaultSubobject<USphereComponent>(TEXT("HitboxPotOfWater"));

	// Set capsule size. Should match the player's range
	SphereComp->InitSphereRadius(size);
	SphereComp->SetCollisionProfileName(TEXT("Trigger"));
	SphereComp->SetupAttachment(RootComponent);

	// Automatically call the corresponding functions when other hitboxes overlap with the trigger capsule.
	SphereComp->OnComponentBeginOverlap.AddDynamic(this, &ApotOfWater::OnOverlapBegin);
}

void ApotOfWater::BeginPlay()
{
	Super::BeginPlay();
}


UFUNCTION()
void ApotOfWater::OnOverlapBegin(UPrimitiveComponent *OverlappedComp, AActor *OtherActor, UPrimitiveComponent *OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult)
{
	if(OtherActor->IsA(ARobotCharacter::StaticClass())){
		a = Cast<ARobotCharacter>(OtherActor);
	}
}


void ApotOfWater::PressButton() {
	if (a) {
		if (Cast<ARobotCharacter>(a)->getCanInteractWithPotOfWater()) {
			if (!onlyOnce) {
				AAbstractButton::PressButton();
				onlyOnce = true;
			}
		}
	}
}

void ApotOfWater::setOnlyOnce(bool onlyOncevalue) {
	onlyOnce = onlyOncevalue;
}

bool ApotOfWater::getOnlyOnce() {
	return onlyOnce;
}

