// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Activable.h"
#include <vector>
#include "SpeedWalk.generated.h"

/**
 * 
 */
UCLASS()
class MIXED_FEELINGS_CHAR_API ASpeedWalk : public AActivable
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, Category = "Action Trigger Capsule")
	class UBoxComponent* TriggerCapsule;


    UPROPERTY(EditAnywhere)
	float force = 0;

	bool active = false;

	std::vector<AActor *> actorToApplyForce;
	ASpeedWalk();
	public:
	void toggleAction_Implementation() override;
	
	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent *OverlappedComp, AActor *OtherActor, 
	UPrimitiveComponent *OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult);
// Unset the current button to activate when player gets close
	UFUNCTION()
	void OnOverlapEnd(UPrimitiveComponent *OverlappedComp, AActor *OtherActor, UPrimitiveComponent *OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
	void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	float GetForce()const {
		return force;
	}


	UFUNCTION(BlueprintCallable)
	void InvertDirection();

	UFUNCTION(BlueprintCallable)
	void SetForce (float newForce);

	void On_Implementation() override;
	void Off_Implementation() override ;

	UFUNCTION(BlueprintCallable, Category = "SpeedWalk")
		bool IsActive() const;
};
