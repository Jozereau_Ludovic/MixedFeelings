// Fill out your copyright notice in the Description page of Project Settings.

#include "DisgustFeeling.h"
#include "Engine.h"
#include "utils/RayCast.h"
#include "object/Plant.h"
#include "RobotCharacter.h"

void UDisgustFeeling::PassiveEffect()
{
}
void UDisgustFeeling::ActiveEffect1()
{

	FHitResult Hit;
	if (RayCast::RayCastFromPlayerPointOfView(Cast<ARobotCharacter>(player), Hit))
	{
		if (Hit.GetActor()->GetClass()->IsChildOf(APlant::StaticClass()))
		{
			Cast<APlant>(Hit.GetActor())->Destruct();

		}
	}
}
void UDisgustFeeling::ActiveEffect2()
{
}
