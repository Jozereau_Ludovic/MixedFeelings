// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbstractButton.h"
#include "TargetButton.generated.h"

/**
 * 
 */
UCLASS()
class MIXED_FEELINGS_CHAR_API ATargetButton : public AAbstractButton
{
	GENERATED_BODY()

	public:
	UFUNCTION(BlueprintCallable, Category = "Buttons")
	void PressButton() override;
	
	
	
};
