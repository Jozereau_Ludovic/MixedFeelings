// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "object/DestructibleObject.h"
#include "Plant.generated.h"

/**
 * 
 */
UCLASS()
class MIXED_FEELINGS_CHAR_API APlant : public ADestructibleObject
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnyWhere, Category = "object")
	class UStaticMeshComponent *SM;
	UPROPERTY(VisibleAnyWhere, Category = "object")
	class UStaticMeshComponent * zone;

	
	float zSize = 100;
	static float constexpr maxSize = 1100;
public:
	UPROPERTY(BlueprintReadOnly)
	bool needGrowing = false;
	
 	void BeginPlay() override;

	UFUNCTION(BlueprintCallable)
	void Grow();

	void Tick(float DeltaTime) override;
	
	
};
