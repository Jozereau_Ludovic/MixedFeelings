// Fill out your copyright notice in the Description page of Project Settings.
#include "Plant.h"
#include "Components/StaticMeshComponent.h"

void APlant::Grow()
{
    needGrowing = true;
}

void APlant::BeginPlay()
{
    Super::BeginPlay();
    SM = Cast<UStaticMeshComponent>(GetRootComponent()->GetChildComponent(0));
	zone = Cast<UStaticMeshComponent>(GetRootComponent()->GetChildComponent(1));
}

void APlant::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    if (needGrowing)
    {
        zSize += 20;
        FVector translate = SM->GetComponentLocation() + FVector{0, 0, 1};
		FVector translate2 = zone->GetComponentLocation() + FVector{ 0,0, 5 };
        SM->SetWorldLocation(translate);
		zone->SetWorldLocation(translate2);
        SM->SetWorldScale3D(FVector{ 200, 200, zSize});
		zone->SetWorldScale3D(FVector{ 2.5, 2.5, zSize });
        if(zSize > maxSize){
            needGrowing = false;
        }
    }
}