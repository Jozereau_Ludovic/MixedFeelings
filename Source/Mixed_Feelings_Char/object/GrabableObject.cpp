// Fill out your copyright notice in the Description page of Project Settings.

#include "GrabableObject.h"
#include "Engine.h"

// Sets default values
AGrabableObject::AGrabableObject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Hitbox = CreateDefaultSubobject<UBoxComponent>(TEXT("Button Hitbox"));
	///Hitbox->InitSphereRadius(1.0f);
	Hitbox->SetupAttachment(RootComponent);
	Hitbox->ShapeColor = FColor::Red;
	Hitbox->SetVisibility(true);
	
}

// Called when the game starts or when spawned
void AGrabableObject::BeginPlay()
{
	Super::BeginPlay();
	SM = Cast<UStaticMeshComponent>(GetRootComponent()->GetChildComponent(0));
    //If the static mesh is valid apply the given force
  	UPrimitiveComponent *a = Cast<UPrimitiveComponent> (GetRootComponent());
	a->SetSimulatePhysics( true ); 
	
}
void AGrabableObject::launch(FVector force){
	SM->AddForce(force);
}

void AGrabableObject::ChangeGravity(){
	hasGravity = !hasGravity;
	UPrimitiveComponent *a = Cast<UPrimitiveComponent> (GetRootComponent());
    a->SetSimulatePhysics( hasGravity ); 
}

// Called every frame
void AGrabableObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

