// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Activable.h"
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"
#include "RemovableObject.generated.h"

UCLASS()
class MIXED_FEELINGS_CHAR_API ARemovableObject : public AActivable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	UPROPERTY(BlueprintReadOnly, Category = "isRemove")
	bool removed = false;
	ARemovableObject();

	UStaticMeshComponent * mesh = nullptr;
	UPROPERTY(EditAnywhere, Category = "HitBoxForOverlap")
	UBoxComponent * hitBox = nullptr;
	
	int actorOverLap = 0;
	bool needChangeState = false;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, Category = "Removable")
	virtual void ChangeState();

	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, 
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) ;

	// Used to stop making buttons selectable when the player goes away
	UFUNCTION()
	void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) ;

	void toggleAction_Implementation() override;

	void On_Implementation() override;
	void Off_Implementation() override ;
	
};
