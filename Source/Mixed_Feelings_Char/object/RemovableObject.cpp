// Fill out your copyright notice in the Description page of Project Settings.

#include "RemovableObject.h"
#include "Engine/EngineTypes.h"

// Sets default values
ARemovableObject::ARemovableObject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
		hitBox = CreateDefaultSubobject<UBoxComponent>(TEXT("Hitbox"));
		hitBox->SetupAttachment(RootComponent);
		hitBox->ShapeColor = FColor::Red;
		hitBox->SetVisibility(true);
		hitBox->SetCollisionProfileName(TEXT("Trigger"));
		hitBox->OnComponentBeginOverlap.AddDynamic(this, &ARemovableObject::OnOverlapBegin);
		hitBox->OnComponentEndOverlap.AddDynamic(this, &ARemovableObject::OnOverlapEnd);
}

// Called when the game starts or when spawned
void ARemovableObject::BeginPlay()
{
	Super::BeginPlay();
	mesh = Cast<UStaticMeshComponent>(GetRootComponent()->GetChildComponent(1));
}

// Called every frame
void ARemovableObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if(needChangeState && !actorOverLap){
		ChangeState();
		needChangeState = false;
	}

}

void ARemovableObject::ChangeState(){
	
		// mesh->SetVisible(false);
		// mesh->SetC
	removed = !removed;
	SetActorHiddenInGame(removed);
	if(mesh){
		if(removed){
			mesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
		}else{
			mesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);			
		}
	}
	
}
void ARemovableObject::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, 
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) {
		actorOverLap++;
}

	// Used to stop making buttons selectable when the player goes away
void ARemovableObject::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) {
	actorOverLap--;
}

void ARemovableObject::toggleAction_Implementation(){
	if(!removed || (removed && !actorOverLap)){
		ChangeState();
	}else if (removed && actorOverLap){
		needChangeState = true;
	}
}

void ARemovableObject::On_Implementation() {
    //Super::On();
	toggleAction();
}
void ARemovableObject::Off_Implementation() {
    //Super::On();
	toggleAction();
}