// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GrabableObject.generated.h"

UCLASS()
class MIXED_FEELINGS_CHAR_API AGrabableObject : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGrabableObject();

	
	UPROPERTY(VisibleAnywhere, Category = "object")
	class UBoxComponent* Hitbox;
	UPROPERTY(VisibleAnyWhere, Category = "object")
	class UStaticMeshComponent* SM;

	bool hasGravity = true;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void ChangeGravity();

	void launch(FVector force);

	
	
};
