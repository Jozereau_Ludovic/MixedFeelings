// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/SkeletalMeshActor.h"
#include "Activable.generated.h"

/**
 * 
 */
UCLASS()
class MIXED_FEELINGS_CHAR_API AActivable : public ASkeletalMeshActor
{
	GENERATED_BODY()
protected:
	bool activation = false;

public:
	AActivable();
	// Sets default values for this actor's properties
	UFUNCTION(BlueprintNativeEvent)
	void toggleAction();
	UFUNCTION(BlueprintNativeEvent)
	void On();
	UFUNCTION(BlueprintNativeEvent)
	void Off();


	virtual void toggleAction_Implementation();
	virtual void On_Implementation();
	virtual void Off_Implementation();
};
