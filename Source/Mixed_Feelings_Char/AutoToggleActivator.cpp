// Fill out your copyright notice in the Description page of Project Settings.

#include "AutoToggleActivator.h"


void AAutoToggleActivator::Tick(float DeltaTime){
	if (!oneTime) {
		Super::Tick(DeltaTime);
		timer -= DeltaTime;

		if (timer <= 0) {
			PressButton();
			timer = autoTimer;
		}
	}
	else if (!wasPressed) {
		PressButton();

		wasPressed = true;
	}
   
}

AAutoToggleActivator::AAutoToggleActivator()
{
	PrimaryActorTick.bCanEverTick = true;
}
