// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"

#include "Components/CapsuleComponent.h"
#include "AbstractButton.h"
#include "Blueprint/UserWidget.h"
#include "object/GrabableObject.h"
#include "object/Plant.h"
#include <vector>
#include "RobotCharacter.generated.h"

UCLASS()
class MIXED_FEELINGS_CHAR_API ARobotCharacter : public ACharacter
{
	GENERATED_BODY()

	// BUTTONS //
	// A "hitbox" around the player that helps to know if he is in range to use buttons
	UPROPERTY(VisibleAnywhere, Category = "Action Trigger Capsule")
	class UCapsuleComponent* TriggerCapsule;

	// The button that will be affected if the player presses the action key
	class AAbstractButton* CurrentButton;

	bool invisible = false;	
	bool dash = false;
	float maxTimeDashing = 0.05; 
	static std::vector<int> characInit;
	bool canWalk = true;

	bool grabAllowed = true;

public:
	UPROPERTY(BlueprintReadOnly)
	class UCharacteristics * characteristics;

	class AGrabableObject * ObjectGrab = nullptr;
private:
	int impulse_value = 500000;
	float maxSpeed;

	bool mustChange = true;

public:
	// Sets default values for this character's properties
	ARobotCharacter();
	~ARobotCharacter();

protected:
	UPROPERTY(EditDefaultsOnly,Category="ActorSpawning")
	TSubclassOf<APlant> PlantBP;

	UPROPERTY(EditDefaultsOnly, Category = "ActorGrowing")
	TSubclassOf<APlant> PlantToGrowBP;
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called when the player presses the action key to interact with objects in his environement
	void OnAction();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	int weight = 300;
	bool canWalkOnWater = false;
	void Grab(AGrabableObject * grabable);

	void UnGrab();

	void ChangeMaxSpeed(float value);

	bool HasObject();

	void Launch();

	void SpawnPlant(FTransform const & pos);

	void SpawnPlantToGrow(FTransform const & pos);

	void Dash(float delta);

	void BeginDash();

	UFUNCTION(BlueprintCallable)
		bool IsDashing() const;

	void setImpulseValue(int impulse_value_set);

	UFUNCTION(BlueprintCallable)
		void setCanWalkOnWater(bool new_value);

	UFUNCTION(BlueprintCallable)
		bool getCanWalkOnWater();

	UFUNCTION(BlueprintCallable)
	int getPlayerWeight();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable )
	 void BeInvisible() ;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable )
	 void BeVisible() ;

	 UFUNCTION(BlueprintNativeEvent, BlueprintCallable )
	 void CanPassThroughWall() ;

	 UFUNCTION(BlueprintNativeEvent, BlueprintCallable )
	 void CannotPassThroughWall() ;

	 UFUNCTION(BlueprintCallable)
	 bool hasFeeling() const;

	// Handles input for moving forward and backward.
	UFUNCTION()
		void MoveForward(float Value);

	// Handles input for moving right and left.
	UFUNCTION()
		void MoveRight(float Value);
	// FPS camera.
	UPROPERTY(VisibleAnywhere)
		class UCameraComponent* FPSCameraComponent;

	// Sets jump flag when key is pressed.
	UFUNCTION()
		void StartJump();

	// Clears jump flag when key is released.
	UFUNCTION()
		void StopJump();

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UIEmotion")
	TSubclassOf<class UUserWidget> wMainMenu;

	UFUNCTION(BlueprintCallable)
	void setCanWalk(bool walk);

	 UFUNCTION( BlueprintCallable, Category = "ZZZ")
	void emotion1();
	 UFUNCTION( BlueprintCallable, Category = "ZZZ")
	void emotion2();
	 UFUNCTION( BlueprintCallable, Category = "ZZZ")
	void emotion3();
	 UFUNCTION( BlueprintCallable, Category = "ZZZ")
	void emotion4();
	 UFUNCTION( BlueprintCallable, Category = "ZZZ")
	void emotion5();

	// First-person mesh (arms), visible only to the owning player.
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
		USkeletalMeshComponent* FPSMesh;

	// BUTTONS //
	// Processes action when the action key is pressed.
	UFUNCTION()
		void Action();

	// Used to make buttons selectable when the player approaches
	UFUNCTION()
		void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	// Used to stop making buttons selectable when the player goes away
	UFUNCTION()
		void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) ;

	UFUNCTION()
		bool IsInvisible() const;

	void ChangeVisibility();

	void GrabOrUnGrab();

	void canGrab(bool can);

	void AddYaw(float rate);

	void AddPitch(float rate);

	void setWeight(float newWeight){
		weight = newWeight;
	}

	bool canInteractWithPotOfWater = false;
	void setCanInteractWithPotOfWater(bool value_canInteractWithPotOfWater);
	bool getCanInteractWithPotOfWater();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void ChangeFeelingsUI(int emotion);
	// Overdose-related game over
	UFUNCTION(BlueprintNativeEvent, Category = "GameOver")
		void OverdoseGameOver(const FString& gameOverMessage);

	UFUNCTION(BlueprintCallable, Category = "Level")
		void InitCharacsForLevel(int joy, int anger, int sadness, int fear, int disgust);
};
