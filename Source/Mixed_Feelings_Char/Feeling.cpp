// Fill out your copyright notice in the Description page of Project Settings.

#include "Feeling.h"

UFeeling::UFeeling(ACharacter * player): player(player) {
	SetFeelingLevel(4);
}

void UFeeling::SetFeelingLevel(int level) {
	feelingLevel = level;

	if (feelingLevel >= 6) {
		overdose = true;
	}
	else {
		overdose = false;
	}
}

int const & UFeeling::GetFeelingLevel()const {
	return feelingLevel;
}

bool const & UFeeling::GetOverdose()const {
	return overdose;
}