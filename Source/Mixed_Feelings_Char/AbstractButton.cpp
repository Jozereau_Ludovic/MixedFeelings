// Fill out your copyright notice in the Description page of Project Settings.

#include "AbstractButton.h"
#include "Engine.h" // For debug purposes
#include "speedWalk.h"

// Sets default values
AAbstractButton::AAbstractButton()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ButtonHitbox = CreateDefaultSubobject<USphereComponent>(TEXT("Button Hitbox"));
	ButtonHitbox->InitSphereRadius(size);
	ButtonHitbox->SetupAttachment(RootComponent);
	ButtonHitbox->ShapeColor = FColor::Red;
	ButtonHitbox->SetVisibility(true);
	ButtonHitbox->bEditableWhenInherited = true;


}

// Called when the game starts or when spawned
void AAbstractButton::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AAbstractButton::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AAbstractButton::PressButton()
{
	
	for(int i = 0; i < maxButtons ; i++){
		if(activatableObject[i]){
			if (inverter){
				auto sw = Cast<ASpeedWalk>(activatableObject[i]);
				if(sw){
					sw->InvertDirection();
				} else{
					activatableObject[i]->toggleAction();
				}
			} else {
			
				activatableObject[i]->toggleAction();
			}
		}
	}
	doSomethingAfterPress();
	
}


void AAbstractButton::doSomethingAfterPress_Implementation(){
	
}
