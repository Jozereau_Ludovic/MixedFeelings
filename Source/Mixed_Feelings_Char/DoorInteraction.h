// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "AbstractButton.h"
#include "DoorInteraction.generated.h"

UCLASS()
class MIXED_FEELINGS_CHAR_API ADoorInteraction : public AAbstractButton
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	ADoorInteraction();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, Category = "Buttons")
	void PressButton() override;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ButtonDoor)
		bool openDoor;
	
};
