// Fill out your copyright notice in the Description page of Project Settings.

#include "TimerButton.h"



void ATimerButton::Tick(float DeltaTime){
    
    if(activate){
        time += DeltaTime;
        if(time >= timeActivated){
            AAbstractButton::PressButton();
            activate = !activate;
        }
    }
}

void ATimerButton::PressButton(){
    if(!activate){
        AAbstractButton::PressButton();
        activate = !activate;
        time = 0;
    }
}


