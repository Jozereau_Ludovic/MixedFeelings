// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Activable.h"
#include "AbstractButton.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/Actor.h"
#include "Camera.generated.h"

UCLASS()
class MIXED_FEELINGS_CHAR_API ACamera : public AActivable
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, Category = "Button")
	class AAbstractButton *Button;

	bool active = true;
	bool triggered = false;

public:	
	// Sets default values for this actor's properties
	ACamera();
	void toggleAction_Implementation() override;
	void On_Implementation() override;
	void Off_Implementation() override;

	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent *OverlappedComp, AActor *OtherActor,
		UPrimitiveComponent *OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult);

	UFUNCTION()
	void OnOverlapEnd(UPrimitiveComponent *OverlappedComp, AActor *OtherActor, UPrimitiveComponent *OtherComp, int32 OtherBodyIndex);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, Category = "Camera_BP")
		bool IsActive() const;
	UFUNCTION(BlueprintCallable, Category = "Camera_BP")
		bool IsTriggered() const;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Action Trigger Capsule")
		class UCapsuleComponent *CapsuleTrigger;
};
