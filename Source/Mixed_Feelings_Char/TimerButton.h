// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbstractButton.h"
#include "TimerButton.generated.h"

/**
 * 
 */
UCLASS()
class MIXED_FEELINGS_CHAR_API ATimerButton : public AAbstractButton
{
	GENERATED_BODY()
	protected:
	bool activate = false;	

	UPROPERTY(EditAnywhere,Category="TimeActivated")
	float timeActivated = 5;

	float time = 0;

public:
	void Tick(float DeltaTime) override;
	void PressButton() override;
	
};
