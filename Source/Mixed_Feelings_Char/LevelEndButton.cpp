// Fill out your copyright notice in the Description page of Project Settings.

#include "LevelEndButton.h"

#include "Engine.h"

// Sets default values
ALevelEndButton::ALevelEndButton()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ALevelEndButton::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ALevelEndButton::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// When pressed, the EndLevel buttons end the level
void ALevelEndButton::PressButton_Implementation() {
	// End level
	Super::doSomethingAfterPress();

}
