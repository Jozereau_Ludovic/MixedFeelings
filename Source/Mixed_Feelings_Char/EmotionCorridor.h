// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Characteristics.h"
#include "EmotionCorridor.generated.h"

UCLASS()
class MIXED_FEELINGS_CHAR_API AEmotionCorridor : public AActor
{
	GENERATED_BODY()
	
public:	

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite)
	Feelings first = Feelings::NONE;

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite)
	Feelings second = Feelings::NONE;

	

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Action Trigger Capsule")
	class USphereComponent* capsuleActivation;

	
	

	// Sets default values for this actor's properties
	AEmotionCorridor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;



	UFUNCTION(BlueprintCallable)
	void apply(UCharacteristics * characteritics);
		// Used to make buttons selectable when the player approaches
	UFUNCTION()
		void activation(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, 
		int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void exitOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, 
		int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION(BlueprintNativeEvent)
		void active(ARobotCharacter * robot);
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
		void reverse();

	

	
	
};
