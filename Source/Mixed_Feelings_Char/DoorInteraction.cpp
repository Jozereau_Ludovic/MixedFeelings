// Fill out your copyright notice in the Description page of Project Settings.

#include "DoorInteraction.h"
#include "Engine.h" // For debug purposes

// Sets default values
ADoorInteraction::ADoorInteraction() 
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ADoorInteraction::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ADoorInteraction::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ADoorInteraction::PressButton() {
		if (openDoor) {
			openDoor = false;
			// Put up a debug message for five seconds. The -1 "Key" value (first argument) indicates that we will never need to update or refresh this message.
		}
		else {
			// Put up a debug message for five seconds. The -1 "Key" value (first argument) indicates that we will never need to update or refresh this message.
			openDoor = true;
		}
	
	
}
