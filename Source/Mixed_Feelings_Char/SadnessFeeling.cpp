// Fill out your copyright notice in the Description page of Project Settings.

#include "SadnessFeeling.h"
#include "Engine.h"
#include "utils/RayCast.h"
#include "object/Plant.h"
#include "potOfWater.h"
#include "RobotCharacter.h"

void USadnessFeeling::PassiveEffect()
{
	Cast<ARobotCharacter>(player)->setWeight(300);
	Cast<ARobotCharacter>(player)->setCanInteractWithPotOfWater(true);
	

}
void USadnessFeeling::ActiveEffect1()
{

	FHitResult Hit;
	if (RayCast::RayCastFromPlayerPointOfView(Cast<ARobotCharacter>(player), Hit, 500))
	{
		if (Hit.GetActor())
		{
			if (Hit.GetActor()->GetClass()->IsChildOf(APlant::StaticClass()))
			{
				char check = Hit.GetActor()->GetFName().ToString()[0];
				if ( check == 'B') {
					//Faire apparaitre une plante MyPlant, la selectionner pour la faire pousser.
					FRotator rotator = FRotator{ 0, 0, 0 };
					FVector pos = Hit.GetActor()->GetRootComponent()->GetChildComponent(1)->GetComponentLocation();
					FTransform transform{ rotator, pos, FVector{0, 0, 0} };

					Cast<ARobotCharacter>(player)->SpawnPlantToGrow(transform);
					Cast<APlant>(Hit.GetActor())->Destruct();
				}

			}else if(Hit.GetActor()->GetClass()->IsChildOf(ApotOfWater::StaticClass())){
				auto pot = Cast<ApotOfWater>(Hit.GetActor());
				pot->PressButton();	
			}
		}
	}
}

void USadnessFeeling::StopPassiveEffect()
{
	ARobotCharacter *robot = Cast<ARobotCharacter>(player);
	robot->CannotPassThroughWall();
	robot->canGrab(true);
	Cast<ARobotCharacter>(player)->setCanInteractWithPotOfWater(false);
	can = false;	

}

void USadnessFeeling::ActiveEffect2()
{

	ARobotCharacter *robot = Cast<ARobotCharacter>(player);
	if (!can)
	{
		if (robot->ObjectGrab)
		{
			robot->ObjectGrab->ChangeGravity();
			robot->UnGrab();
		}
		robot->CanPassThroughWall();
		robot->canGrab(false);
	}
	else
	{
		robot->CannotPassThroughWall();
		robot->canGrab(true);
	}
	can = !can;
}
