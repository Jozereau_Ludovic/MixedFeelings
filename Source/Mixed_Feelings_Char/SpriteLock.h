// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SpriteLock.generated.h"


USTRUCT(BlueprintType)
struct MIXED_FEELINGS_CHAR_API FSpriteLock 
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPROPERTY(BlueprintReadOnly)
		int numberSpriteMax = 3;
	UPROPERTY(BlueprintReadWrite)
		int numberSprite = 0;
	UPROPERTY(BlueprintReadOnly)
		float delta = 0.5;
	UPROPERTY(BlueprintReadWrite)
		float deltaCurrent = 0;
	UPROPERTY(BlueprintReadWrite)
		bool needChange = false;



		
	
};
