// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "water.generated.h"

UCLASS()
class MIXED_FEELINGS_CHAR_API Awater : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	Awater();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
