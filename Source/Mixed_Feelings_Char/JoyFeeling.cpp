// Fill out your copyright notice in the Description page of Project Settings.

#include "JoyFeeling.h"
#include "Engine.h"
#include "UObject/UObjectGlobals.h"
#include "utils/RayCast.h"


UJoyFeeling::UJoyFeeling(ACharacter * player):UFeeling(player){

}
void UJoyFeeling::PassiveEffect(){
	player->GetCharacterMovement()->GravityScale -= 0.7;
	Cast<ARobotCharacter>(player)->setWeight(0);
}
void UJoyFeeling::StopPassiveEffect(){
	player->GetCharacterMovement()->GravityScale += 0.7;
}
void UJoyFeeling::ActiveEffect1(){
	player->Jump();
	
}
void UJoyFeeling::ActiveEffect2(){
	FHitResult Hit;
	
	if (RayCast::RayCastFromPlayerPointOfView(Cast<ARobotCharacter>(player), Hit, 500))
	{
		FRotator rotator;
		if (Cast<AGround>(Hit.GetActor()))
		{
			FVector pos = Hit.ImpactPoint;
			if(Hit.GetActor()->GetActorRotation() != FRotator{0, 0, 0}){
				pos += 100*Hit.ImpactNormal;
			}else{
					rotator = FRotator{0, player->Controller->GetControlRotation().Yaw + 180, 0};
					pos -= FRotator{0, player->Controller->GetControlRotation().Yaw + 180, 0}.Vector()*350;
			}
			FTransform transform{rotator, pos, FVector{1, 1, 1}};
			Cast<ARobotCharacter>(player)->SpawnPlant(transform);
			
		}
	}
}

