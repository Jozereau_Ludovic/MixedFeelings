// Fill out your copyright notice in the Description page of Project Settings.

#include "Activable.h"
#include "Engine.h" // For debug purposes


AActivable::AActivable(){
	PrimaryActorTick.bCanEverTick = true;

}
void AActivable::toggleAction_Implementation() {

	if (activation) {
		Off();
		
	}
	else {
		On();
	
	}
	activation = !activation;

}

void AActivable::On_Implementation() {
	activation = true;
}

void AActivable::Off_Implementation() {
	activation = false;
}