// Fill out your copyright notice in the Description page of Project Settings.

#include "BossButton.h"


void ABossButton::Tick(float DeltaTime){
    
    if(activate){
        time += DeltaTime;
        if(time >= timeActivated){
            AAbstractButton::PressButton();
            activate = !activate;

            if(state == 0){
                for(int i = 0; i < maxButtons ; i++){
                    if(activatableObject1[i]){
                        activatableObject1[i]->toggleAction();
                    }
                }
            }
        
                
            
        if(state == 1){
                for(int i = 0; i < maxButtons ; i++){
                    if(activatableObject2[i]){
                        activatableObject2[i]->toggleAction();
                    }
                }
            }
            if(state == 2){
                for(int i = 0; i < maxButtons ; i++){
                    if(activatableObject3[i]){
                        activatableObject3[i]->toggleAction();
                    }
                }
            }
            ++state;
        }
        
    }
    
}

