// Fill out your copyright notice in the Description page of Project Settings.

#include "AngerFeeling.h"
#include "Engine.h"
#include "RobotCharacter.h"
#include "utils/RayCast.h"
#include "RobotCharacter.h"

void UAngerFeeling::PassiveEffect()
{
	player->GetCharacterMovement()->GravityScale += 1;
	Cast<ARobotCharacter>(player)->setWeight(600);
	
}
void UAngerFeeling::StopPassiveEffect()
{
	player->GetCharacterMovement()->GravityScale -= 1;
	Cast<ARobotCharacter>(player)->setWeight(600);
	auto robot = Cast<ARobotCharacter>(player);
	if (robot->ObjectGrab && Cast<UPrimitiveComponent>(robot->ObjectGrab->GetRootComponent())->GetMass() >= 300)
	{
		robot->ObjectGrab->ChangeGravity();
		robot->UnGrab();
	}
}
void UAngerFeeling::ActiveEffect1()
{
	ARobotCharacter *robot = Cast<ARobotCharacter>(player);
	

	if (robot->HasObject())
	{
		robot->Launch();
	}
	else
	{
		FHitResult Hit;
		
		if (RayCast::RayCastFromPlayerPointOfView(robot, Hit))
		{
			auto test = Cast<AGrabableObject>(Hit.GetActor());
			if(Hit.GetActor()){
				if (Hit.GetActor()->GetClass()->IsChildOf(AGrabableObject::StaticClass()))
				{
				
					robot->Grab(Cast<AGrabableObject>(Hit.GetActor()));
				}
			}
			
		}
	}
}

void UAngerFeeling::ActiveEffect2()
{
	ARobotCharacter *robot = Cast<ARobotCharacter>(player);
	robot->setImpulseValue(200000);
	robot->BeginDash();
}
