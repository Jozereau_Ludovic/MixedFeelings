// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Feeling.h"
#include "object/Ground.h"
#include "Sound/SoundCue.h"
#include "Components/AudioComponent.h"
#include "ConstructorHelpers.h"
/**
 * 
 */
class MIXED_FEELINGS_CHAR_API UJoyFeeling : public UFeeling
{

	
public:
	UJoyFeeling(ACharacter * player);
	virtual void PassiveEffect() override;
	virtual void StopPassiveEffect() override;
	virtual void ActiveEffect1() override;
	virtual void ActiveEffect2() override;
	
	
};
