// Fill out your copyright notice in the Description page of Project Settings.

#include "Fan.h"
#include "RobotCharacter.h"
#include "Engine.h"
AFan::AFan()
{
    // Add Buttons detection hitbox
    TriggerCapsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Trigger Capsule"));
    // Set capsule size. Should match the player's range
    TriggerCapsule->InitCapsuleSize(55.f, 96.f);
    TriggerCapsule->SetCollisionProfileName(TEXT("Trigger"));
    TriggerCapsule->SetupAttachment(RootComponent);
    // Automatically call the corresponding functions when other hitboxes overlap with the trigger capsule.
    TriggerCapsule->OnComponentBeginOverlap.AddDynamic(this, &AFan::OnOverlapBegin);
    TriggerCapsule->OnComponentEndOverlap.AddDynamic(this, &AFan::OnOverlapEnd);
}
void AFan::toggleAction_Implementation()
{
   // Super::toggleAction();
    active = !active;
}

// Set the current button to activate when player gets close
void AFan::OnOverlapBegin(UPrimitiveComponent *OverlappedComp, AActor *OtherActor, UPrimitiveComponent *OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult)
{
    actorToApplyForce.emplace_back(OtherActor);
}

// Unset the current button to activate when player gets close
void AFan::OnOverlapEnd(UPrimitiveComponent *OverlappedComp, AActor *OtherActor, UPrimitiveComponent *OtherComp, int32 OtherBodyIndex)
{
    auto it = std::find(actorToApplyForce.begin(), actorToApplyForce.end(), OtherActor);
    if (it != actorToApplyForce.end())
    {
        actorToApplyForce.erase(it);
    }
}

void AFan::Tick(float DeltaTime)
{

    if (active)
    {
        for (auto actor : actorToApplyForce)
        {
            auto a = Cast<UStaticMeshComponent>(actor->GetRootComponent()->GetChildComponent(0));
            if (a)
            {
                //a->AddForce(FVector(0, 0, force*10));
                a->AddForce(GetRootComponent()->GetUpVector()*force);
            }else{
                 auto a = Cast<ARobotCharacter>(actor);
                if (a)
                {
                    //a->GetCharacterMovement()->AddForce(FVector(0, 0, force));
                    a->GetCharacterMovement()->AddForce(GetRootComponent()->GetUpVector()*force);
                }
            }
        }
    
    }
}
void AFan::On_Implementation() {
    //Super::On();
    active = true;
}
void AFan::Off_Implementation() {
    //Super::On();
    active = false;
    
}