// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbstractButton.h"
#include "AutoToggleActivator.generated.h"

/**
 * 
 */
UCLASS()
class MIXED_FEELINGS_CHAR_API AAutoToggleActivator : public AAbstractButton
{
	GENERATED_BODY()

	public:
		UPROPERTY(EditAnywhere, Category = "Timer")
			bool oneTime;
		
		bool wasPressed = false;

		UPROPERTY(EditAnywhere, Category = "Timer")
		int autoTimer = 10;

		void Tick(float DeltaTime) override;
		AAutoToggleActivator();
	private:
			int timer = 10;
	
};
