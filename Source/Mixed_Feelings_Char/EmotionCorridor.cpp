// Fill out your copyright notice in the Description page of Project Settings.

#include "EmotionCorridor.h"
#include "Components/SphereComponent.h"

// Sets default values
AEmotionCorridor::AEmotionCorridor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	 capsuleActivation = CreateDefaultSubobject<USphereComponent>(TEXT("Trigger Capsule"));
	// Set capsule size. Should match the player's range
	 capsuleActivation->SetCollisionProfileName(TEXT("Trigger"));
	 capsuleActivation->SetupAttachment(RootComponent);
	// Automatically call the corresponding functions when other hitboxes overlap with the trigger capsule.
	 capsuleActivation->OnComponentBeginOverlap.AddDynamic(this, &AEmotionCorridor::activation);


	
	
}

// Called when the game starts or when spawned
void AEmotionCorridor::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AEmotionCorridor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}


// Used to make buttons selectable when the player approaches

void AEmotionCorridor::activation(UPrimitiveComponent *OverlappedComp, AActor *OtherActor, UPrimitiveComponent *OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult)
{
	if(Cast<ARobotCharacter>(OtherActor)){
		active(Cast<ARobotCharacter>(OtherActor));
	}
}

void AEmotionCorridor::exitOverlap(UPrimitiveComponent *OverlappedComp, AActor *OtherActor, UPrimitiveComponent *OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult)
{
	//if(Cast<ARobotCharacter>(OtherActor)){
		reverse();
	//}
}
void AEmotionCorridor::apply(UCharacteristics * characteritics){
	bool phil [6] = {0};
	phil[static_cast<int>(first)] = true;
	phil[static_cast<int>(second)] = true;
	

	characteritics->UpdateFeelingsLevel(phil[0], phil[1], phil[2], phil[3], phil[4]);

}

// void AEmotionCorridor::exit(UPrimitiveComponent *OverlappedComp, AActor *OtherActor, UPrimitiveComponent *OtherComp, int32 OtherBodyIndex)
// {
// }
void AEmotionCorridor::active_Implementation(ARobotCharacter * robot){


}
void AEmotionCorridor::reverse_Implementation(){


}
