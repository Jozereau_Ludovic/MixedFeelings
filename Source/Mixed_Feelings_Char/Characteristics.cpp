// Fill out your copyright notice in the Description page of Project Settings.

#include "Characteristics.h"
#include "JoyFeeling.h"
#include "SadnessFeeling.h"
#include "AngerFeeling.h"
#include "FearFeeling.h"
#include "DisgustFeeling.h"
#include "Engine.h"

// Sets default values for this component's properties
UCharacteristics::UCharacteristics()
{
	player = Cast<ARobotCharacter>(GetOwner());

	UFeeling *sadness = new USadnessFeeling(player);
	UFeeling *fear = new UFearFeeling(player);
	UFeeling *ang = new UAngerFeeling(player);
	UFeeling *disg = new UDisgustFeeling(player);

	feelings.push_back(new UJoyFeeling((player)));
	feelings.push_back(ang);
	feelings.push_back(sadness);
	feelings.push_back(fear);
	feelings.push_back(disg);

	sad = false;
}

void UCharacteristics::StopPassive() {
	if (currentFeeling) {
		currentFeeling->StopPassiveEffect();
		currentFeeling = nullptr;
	}
}
UCharacteristics::~UCharacteristics()
{
	for (auto feel : feelings)
	{
		delete feel;
	}
}
bool UCharacteristics::hasFeeling() const
{
	return currentFeeling;
}

void UCharacteristics::ActiveEffect1()
{
	if (canAct)
	{
		if (currentFeeling && (currentFeeling->GetFeelingLevel() >= 2))
		{
			currentFeeling->ActiveEffect1();
		}
	}
}

void UCharacteristics::ActiveEffect2()
{
	if (canAct)
	{
		if (currentFeeling && (currentFeeling->GetFeelingLevel() >= 4))
		{
			currentFeeling->ActiveEffect2();
			//UpdateFeelingsLevel(true,false,false,false,false);
		}
	}
}

bool UCharacteristics::ChangeFeeling(int emotionNumber)
{
	// V�rification de la validit� de l'indice
	if (emotionNumber > 4 || feelings[emotionNumber]->GetFeelingLevel() < 1)
	{
		return false;
	}
	// Stop passive effect
	if (currentFeeling)
	{
		currentFeeling->StopPassiveEffect();
	}

	// Check sadness
	sad = emotionNumber == 2;
	// Check anger
	angry = emotionNumber == 1;
	// Change
	currentFeeling = feelings[emotionNumber];
	currentFeeling->PassiveEffect();

	return true;
}
void UCharacteristics::setCanAct(bool can)
{
	canAct = can;
}

// Parameters : TRUE if given emotion level should INCREASE and FALSE if it should DECREASE
// Updates the feelings level when entering a corridor. Calls GameOver is overdose is reached.
void UCharacteristics::UpdateFeelingsLevel(bool joy, bool ang, bool sadness, bool fear, bool disgust)
{
	int diffValue = 2;
	int sign;
	bool overdoseReached = false;

	// Sadness's passive divides all corridor effects by 2.
	if (sad)
	{
		diffValue = 1;
	}

	// Use a vector of same dimension as emotions to remember the sign of assignement operation
	std::vector<bool> emotionsFactors;
	emotionsFactors.push_back(joy);
	emotionsFactors.push_back(ang);
	emotionsFactors.push_back(sadness);
	emotionsFactors.push_back(fear);
	emotionsFactors.push_back(disgust);

	for (int i = 0; i < 5; i++)
	{
		// Translate boolean to sign factor
		auto &feel = feelings[i];
		if (emotionsFactors[i] == false)
		{
			sign = -1;
		}
		else
		{
			sign = 1;
		}
		// Update feeling value
		feel->SetFeelingLevel(feel->GetFeelingLevel() + diffValue * sign);
		if (feel->GetFeelingLevel() < 0)
		{
			feel->SetFeelingLevel(0);
		}

		// Reuse same vector to see if there is an overdose
		emotionsFactors[i] = feel->GetOverdose();
		if (emotionsFactors[i] == true)
		{
			overdoseReached = true;
		}
	}

	if (overdoseReached)
	{
		player->OverdoseGameOver(GetEmotionsNames(emotionsFactors));
	}

	emotionStillAvaiblable();
}

// Should be used only when teleporting the character to a new level. Sets all feelings level to the given values.
void UCharacteristics::InitFeelingsLevel(int joy, int ang, int sadness, int fear, int disgust)
{
	feelings[0]->SetFeelingLevel(joy);
	feelings[1]->SetFeelingLevel(ang);
	feelings[2]->SetFeelingLevel(sadness);
	feelings[3]->SetFeelingLevel(fear);
	feelings[4]->SetFeelingLevel(0);
	
}

void UCharacteristics::emotionStillAvaiblable()
{
	if ((currentFeeling && currentFeeling->GetFeelingLevel() == 0) || !currentFeeling)
	{
		int i = 0;
		//currentFeeling = nullptr;
		for (int j = 0; j < 4; j++)
		{
			if (feelings[j]->GetFeelingLevel() != 0)
			{
				//ChangeFeeling(i);
				player->ChangeFeelingsUI(i);
				return;
			}
			i++;
		}
		currentFeeling->StopPassiveEffect();
		currentFeeling = nullptr;
	}
}

int const &UCharacteristics::GetJoyLevel() const
{
	return feelings[static_cast<int>(Feelings::JOY)]->GetFeelingLevel();
}
int const &UCharacteristics::GetAngerLevel() const
{
	return feelings[static_cast<int>(Feelings::ANGER)]->GetFeelingLevel();
}
int const &UCharacteristics::GetSadnessLevel() const
{
	return feelings[static_cast<int>(Feelings::SADNESS)]->GetFeelingLevel();
}
int const &UCharacteristics::GetFearLevel() const
{
	return feelings[static_cast<int>(Feelings::FEAR)]->GetFeelingLevel();
}
int const &UCharacteristics::GetDisgustLevel() const
{
	return feelings[static_cast<int>(Feelings::DISGUST)]->GetFeelingLevel();
}
bool const UCharacteristics::IsAngry() const
{
	return angry;
}
void UCharacteristics::AddFeeling(Feelings feeling)
{
	feelings[static_cast<int>(feeling)]->SetFeelingLevel(2);
	//emotionStillAvaiblable();
}
bool UCharacteristics::IsSad() const
{
	return sad;
}

// Translates the vector of 5 booleans to the corresponding emotions names (in French)
FString UCharacteristics::GetEmotionsNames(std::vector<bool> emotions) const
{
	FString message = TEXT("Overdose de ");
	bool firstFound = false;
	int emotionCounter = 0;

	while (emotionCounter < 5)
	{
		if (emotions[emotionCounter] == true)
		{
			if (firstFound)
			{
				message += TEXT(", ");
			}
			else
			{
				firstFound = true;
			}
			switch (emotionCounter)
			{
			case 0:
				message += TEXT("joie");
				break;
			case 1:
				message += TEXT("colère");
				break;
			case 2:
				message += TEXT("tristesse");
				break;
			case 3:
				message += TEXT("peur");
				break;
			case 4:
				message += TEXT("dégoût");
				break;
			default:
				break;
			}
		}
		emotionCounter++;
	}

	return message;
}