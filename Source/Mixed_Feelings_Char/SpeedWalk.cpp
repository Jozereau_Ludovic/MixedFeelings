// Fill out your copyright notice in the Description page of Project Settings.

#include "SpeedWalk.h"
#include "RobotCharacter.h"
#include "Engine.h"



ASpeedWalk::ASpeedWalk()
{
    // Add Buttons detection hitbox
    TriggerCapsule = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger Capsule"));
    // Set capsule size. Should match the player's range
    TriggerCapsule->SetCollisionProfileName(TEXT("Trigger"));
    TriggerCapsule->SetupAttachment(RootComponent);
    // Automatically call the corresponding functions when other hitboxes overlap with the trigger capsule.
    TriggerCapsule->OnComponentBeginOverlap.AddDynamic(this, &ASpeedWalk::OnOverlapBegin);
    TriggerCapsule->OnComponentEndOverlap.AddDynamic(this, &ASpeedWalk::OnOverlapEnd);
}
void ASpeedWalk::toggleAction_Implementation()
{
    active = !active;
}

// Set the current button to activate when player gets close
void ASpeedWalk::OnOverlapBegin(UPrimitiveComponent *OverlappedComp, AActor *OtherActor, UPrimitiveComponent *OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult)
{
    actorToApplyForce.emplace_back(OtherActor);
}

// Unset the current button to activate when player gets close
void ASpeedWalk::OnOverlapEnd(UPrimitiveComponent *OverlappedComp, AActor *OtherActor, UPrimitiveComponent *OtherComp, int32 OtherBodyIndex)
{
    auto it = std::find(actorToApplyForce.begin(), actorToApplyForce.end(), OtherActor);
    if (it != actorToApplyForce.end())
    {
        actorToApplyForce.erase(it);
    }
}

void ASpeedWalk::Tick(float DeltaTime)
{
    if (active)
    {
        for (auto actor : actorToApplyForce)
        {
            auto a = Cast<UStaticMeshComponent>(actor->GetRootComponent()->GetChildComponent(0));
            if (a)
            {
                a->AddForce(force*GetActorRightVector());
            }else{
                 auto a = Cast<ARobotCharacter>(actor);
                if (a)
                {
                    a->GetCharacterMovement()->AddForce(force*GetActorRightVector());
                }
            }
        }
    
    }
}

void  ASpeedWalk::InvertDirection(){
      force = -force;
	  toggleAction();
	  toggleAction();
}

void  ASpeedWalk::SetForce (float newForce){
    force = newForce;
}

void ASpeedWalk::On_Implementation() {
    //active = true;
    //Super::On();
	toggleAction();
}
void ASpeedWalk::Off_Implementation() {
    //Super::On();
	toggleAction();
    //active = false;
    
}

bool ASpeedWalk::IsActive() const {
	return active;
}