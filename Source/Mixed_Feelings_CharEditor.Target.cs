// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class Mixed_Feelings_CharEditorTarget : TargetRules
{
	public Mixed_Feelings_CharEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;

		ExtraModuleNames.AddRange( new string[] { "Mixed_Feelings_Char" } );
	}
}
